/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica.jsp;

import java.io.Serializable;

/**
 *
 * @author stefan
 */
public class LoginBean implements Serializable {
    
    private String login = null;
    private String senha = null;
    private String perfil = null;

    public LoginBean() {
    }
    
    public boolean isLoginEmBranco(){
        return (login == null || login.isEmpty());
    }
    
    public boolean isLoginValido(){
        if(login == null || senha == null || login.isEmpty() || senha.isEmpty()){
            return false;
        }
        return login.equals(senha);
//        return false;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    
    
}
