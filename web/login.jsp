
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="pratica.jsp.LoginBean"%>
<%@ page import="java.util.Date"%>
<jsp:useBean id="loginBean" class="pratica.jsp.LoginBean" scope="page">
    <jsp:setProperty name="loginBean" property="login" value='<%= request.getParameter("login")%>' />
    <jsp:setProperty name="loginBean" property="senha" value='<%= request.getParameter("senha")%>' />
    <jsp:setProperty name="loginBean" property="perfil" value='<%= request.getParameter("perfil")%>' />
</jsp:useBean>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
            <title>Login</title>
            <style>
                .loginValido{
                    color : blue;
                }
                
                .loginInvalido{
                    color : red;
                }
            </style>
    </head>
    <body>
        <form method="post" action="/pratica-jsp/login.jsp">
            Código: <input type="text" name="login"/><br/>
            Nome: <input type="password" name="senha"/><br/>
            Perfil: <select name="perfil">
                <option value="1">Cliente</option>
                <option value="2">Gerente</option>
                <option value="3">Administrador</option>
            </select>
            <input type="submit" value="Enviar"/>
        </form>
        <% if (loginBean.isLoginValido()) {%>
        <div class="loginValido">
            <%= loginBean.getPerfil()%>, login bem sucedido, para <%= loginBean.getLogin()%> às <%= (new Date()).toString()%>
        </div>
        <% } else if (!loginBean.isLoginEmBranco()) { %>
        <div class="loginInvalido">
            Acesso negado
        </div>
        <% } else { %>
        <!--        <div class="digiteLogin">
                    Por favor, digite um login
                </div>        -->
        <% }%>
    </body>
</html>
